package goap.types;

import goap.State;

/**
 * Simple wrapper over a State->Bool map, to use as an 1D hash-set.
 */
@:forward(exists)
abstract HashSet(Map<State, Bool>)
{
    public function new()
    {
        this = new Map();
    }

    public inline function set(value: State)
    {
        this.set(value, true);
    }
}
