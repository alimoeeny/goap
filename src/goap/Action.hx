package goap;

abstract class Action<S: Int>
{
    /**
     * Set this to `false` to disable this action.
     */
    public var enabled(default, null): Bool = true;

    /**
     * Conditions required for this action to be considered.
     * The action will not be taken while even one bit
     * of the precondition flags is not set in the world state.
     */
    public var preconditions(default, null): S = cast 0;

    /**
     * The state bit flags that are set when this action is performed.
     */
    public var effectSet(default, null): S = cast 0;

    /**
     * The state bit flags that are cleared when this action is performed.
     */
    public var effectClear(default, null): S = cast 0;

    /**
     * The cost of performing this action.
     *
     * This needs to be a positive number, otherwise the results are unspecified.
     */
    public var cost(default, null): Float = 1;

    /**
     * A name for the action, used only when debugging.
     */
    public var name(default, null): String = "action";

    @:noCompletion
    public inline function applyTo(state: State): State
    {
        state.setAndClear(effectSet, effectClear);
        return state;
    }
}
