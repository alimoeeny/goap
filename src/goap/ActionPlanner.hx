package goap;

import astar.Graph;
import astar.SearchResult;
import astar.types.NextNodeCallback;


@:generic class ActionPlanner<S: Int> extends Graph
{
    var actions: Array<Action<S>>;
    var goal: State;

    public function new()
    {
        super();
        actions = [ ];
    }

    /**
     * Set the list of actions that are available to the agent.
     *
     * @param actions the list of actions
     */
    public function setActions(actions: Array<Action<S>>)
    {
        this.actions = actions;
    }

    /**
     * Add an action to the list of the actions that are available to the agent.
     *
     * @param action the action
     */
    public function addAction(action: Action<S>)
    {
        actions.push(action);
    }

    /**
     * Remove an action from the list of the actions that are available to the agent.
     *
     * Note that for temporarily disabling and enabling actions there exists also the `enabled` field.
     *
     * @param action the action
     */
    public function removeAction(action: Action<S>)
    {
        actions.remove(action);
    }

    /**
     * Calculate the shortest action plan that an agent may use to get from one state to another.
     *
     * Note that the `goalState` should have set to `1` the bit flags which are required to be `1` in the state
     * for the goal to have been reached.
     * This means that the other flags that are `0` in the `goalState`, may be either `0` or `1` when actually reaching the goal.
     *
     * @param startState the initial state of the agent
     * @param goalState the goal state of the agent
     * @param maxCost the maximum acceptable cost; specifying this allows the algorithm to exit early with result status `MaxCostExceeded`,
     *                if the cost exceeds the given value during the search
     * @return the action plan as a list of `Action` instances from the actions list
     */
    public function getPlan(startState: S, goalState: S, ?maxCost: Float): ActionPlan<S>
    {
        goal = goalState;

        var actionPlan: ActionPlan<S> =
        {
            result: None,
            cost: 0,
            plan: null
        };

        if (cast(startState, State).hasAll(goalState))
        {
            // the goal conditions given are already fulfilled in the starting state
            actionPlan.result = StartEndSame;
            return actionPlan;
        }

        // run the A* algorithm to find the optimal plan
        var result: SearchResult = astar.solve(startState, goalState, maxCost);
        actionPlan.result = result.result;
        actionPlan.cost = result.cost;
        if (result.result != Solved)
        {
            // not solved
            return actionPlan;
        }

        // a path has been found by the A* algorithm
        // the result.path field contains the list of states - we don't care about that here
        // instead, just get the action corresponding to all transitions
        // skipping the first element because there's no transition into the intitial state
        actionPlan.plan = [ ];
        for (i in 1...result.transitions.length)
        {
            actionPlan.plan.push(actions[ result.transitions[ i ] ]);
        }

        return actionPlan;
    }

    @:noCompletion
    public function getNextNodes(state: Int, callback: NextNodeCallback)
    {
        var state: State = state;

        for (i in 0...actions.length)
        {
            var action: Action<S> = actions[ i ];
            if (!action.enabled)
            {
                continue;
            }

            // check if action is possible
            var preconditions: State = action.preconditions;
            if (!state.hasAll(preconditions))
            {
                continue;
            }

            // compute the next state based on the effect of the action
            var nextState: State = action.applyTo(state);
            if (nextState.hasAll(goal))
            {
                // the A* algorithm expects to find an exact match
                // but for our purposes it is good enough if all of the goal flags are set to `1`
                nextState = goal;
            }

            callback(nextState, action.cost, i);
        }
    }

    @:noCompletion
    public function getHeuristicCost(from: Int, to: Int): Float
    {
        // regress to Dijkstra's algorithm for now
        return 0;
    }
}
