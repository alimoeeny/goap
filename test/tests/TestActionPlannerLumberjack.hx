package tests;

import astar.types.Result;
import goap.Action;
import goap.ActionPlan;
import goap.ActionPlanner;
import haxe.PosInfos;
import types.LumberjackState;
import types.actions.*;
import utest.Assert;
import utest.ITest;


@:access(goap.Action)
class TestActionPlannerLumberjack implements ITest
{
    var planner: ActionPlanner<LumberjackState>;

    var chopTree: Action<LumberjackState>;
    var gatherBranches: Action<LumberjackState>;
    var getAxe: Action<LumberjackState>;
    var nap: Action<LumberjackState>;

    public function new()
    {
    }

    function setup()
    {
        chopTree = new ChopTree();
        gatherBranches = new GatherBranches();
        getAxe = new GetAxe();
        nap = new Nap();

        planner = new ActionPlanner();
        planner.setActions([ chopTree, gatherBranches, getAxe, nap ]);
    }

    function testGetWood()
    {
        var actionPlan: ActionPlan<LumberjackState> = planner.getPlan(Empty, HasWood);

        assertPlanEquals([ nap, getAxe, nap, chopTree ], actionPlan.plan);
    }

    function testGetWoodNapExpensive()
    {
        nap.cost = 100;

        var actionPlan: ActionPlan<LumberjackState> = planner.getPlan(Empty, HasWood);

        assertPlanEquals([ nap, gatherBranches ], actionPlan.plan);
    }

    function testGetWoodNapExpensiveBranchesNotAvailable()
    {
        nap.cost = 100;
        planner.removeAction(gatherBranches);

        var actionPlan: ActionPlan<LumberjackState> = planner.getPlan(Empty, HasWood);
        assertPlanEquals([ nap, getAxe, nap, chopTree ], actionPlan.plan);

        // add it again and retry
        planner.addAction(gatherBranches);
        var actionPlan: ActionPlan<LumberjackState> = planner.getPlan(Empty, HasWood);
        assertPlanEquals([ nap, gatherBranches ], actionPlan.plan);
    }

    function testGetWoodStartRested()
    {
        var actionPlan: ActionPlan<LumberjackState> = planner.getPlan(NotTired, HasWood);

        assertPlanEquals([ getAxe, nap, chopTree ], actionPlan.plan);
    }

    function testGetWoodStartWithAxe()
    {
        var actionPlan: ActionPlan<LumberjackState> = planner.getPlan(HasAxe, HasWood);

        assertPlanEquals([ nap, chopTree ], actionPlan.plan);
    }

    function testInvalidPlan()
    {
        var actionPlan: ActionPlan<LumberjackState> = planner.getPlan(HasAxe, HasAxe);

        Assert.equals(Result.StartEndSame, actionPlan.result);
        Assert.isNull(actionPlan.plan);
    }

    function testPlanCostExceeded()
    {
        var actionPlan: ActionPlan<LumberjackState> = planner.getPlan(Empty, HasWood, 2);

        Assert.equals(Result.MaxCostExceeded, actionPlan.result);
        Assert.isNull(actionPlan.plan);
    }

    function testImpossiblePlan()
    {
        getAxe.enabled = false;

        var actionPlan: ActionPlan<LumberjackState> = planner.getPlan(Empty, HasAxe);

        Assert.equals(Result.NoSolution, actionPlan.result);
        Assert.isNull(actionPlan.plan);
    }

    function assertPlanEquals(expect: Array<Action<LumberjackState>>, actual: Array<Action<LumberjackState>>, ?pos: PosInfos)
    {
        var equal: Bool = expect.length == actual.length;

        if (!equal)
        {
            for (i in 0...expect.length)
            {
                equal = equal && (expect[ i ] == actual[ i ]);
            }
        }

        Assert.isTrue(equal, 'expect ${planToString(expect)} but got ${planToString(actual)}', pos);
    }

    function planToString(plan: Array<Action<LumberjackState>>): String
    {
        return '[ ' + plan.map(a -> a.name).join(', ') + ' ]';
    }
}
