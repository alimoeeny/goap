package types;

import goap.State;


@:build(goap.macro.StateBuilder.build())
@:forward
enum abstract TestState(State)
{
    var State1;
    var State2;
    var State3;
    var State4;
    var State5;

    @:to
    function toState(): State
    {
        return cast(this, State);
    }
}
