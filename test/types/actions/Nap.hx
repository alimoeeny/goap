package types.actions;

import goap.Action;


class Nap extends Action<LumberjackState>
{
    public function new()
    {
        name = "Nap";
        effectSet = NotTired;
    }
}
