package types.actions;

import goap.Action;


class GatherBranches extends Action<LumberjackState>
{
    public function new()
    {
        name = "GatherBranches";
        preconditions = NotTired;
        effectSet = HasWood;
        effectClear = NotTired;
        cost = 8;
    }
}
