package types.actions;

import goap.Action;


class ChopTree extends Action<LumberjackState>
{
    public function new()
    {
        name = "ChopTree";
        preconditions = NotTired | HasAxe;
        effectSet = HasWood;
        effectClear = NotTired;
    }
}
